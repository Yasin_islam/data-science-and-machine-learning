import random
import time


# Function to calculate the cost (number of attacking pairs of queens)
def cost(positions):
    """
    Calculate the cost (number of attacking pairs of queens) in the given positions.

    Args:
    positions (list): List representing the positions of queens on the board.

    Returns:
    int: Number of attacking pairs of queens.
    """
    n = len(positions)
    attack = 0
    for i in range(n):
        for j in range(i + 1, n):
            if positions[i] == positions[j] or abs(positions[i] - positions[j]) == j - i:
                attack += 1
    return attack


# Function to generate a semi-random initial position based on a heuristic
def heuristic_initial_positions(n):
    """
    Generate a semi-random initial position of queens based on a heuristic.

    Args:
    n (int): Size of the chessboard (number of queens).

    Returns:
    list: List representing the semi-random initial positions of queens.
    """
    positions = list(range(1, n + 1))
    random.shuffle(positions)
    return positions


# Function to get initial positions of queens from the user or generate them randomly
def get_initial_positions(n, initial_positions=None):
    """
    Get initial positions of queens from the user, or generate them randomly.

    Args:
    n (int): Size of the chessboard (number of queens).
    initial_positions (list or None): List representing initial positions of queens.
                                      If None, positions are generated randomly.

    Returns:
    list: List representing the initial positions of queens.
    """
    if initial_positions is not None:
        return initial_positions
    else:
        return heuristic_initial_positions(n)  # Generate positions randomly


# Function to get user input for algorithm parameters
def get_algorithm_parameters():
    """
    Get user input for algorithm parameters.

    Returns:
    dict: Dictionary containing algorithm parameters.
    """
    parameters = {}
    parameters['num_scouts'] = int(input("Enter the number of scouts: "))
    parameters['num_best_sites'] = int(input("Enter the number of best sites: "))
    parameters['num_bees_best_sites'] = int(input("Enter the number of bees per best site: "))
    parameters['num_other_sites'] = int(input("Enter the number of other sites: "))
    parameters['num_bees_other_sites'] = int(input("Enter the number of bees per other site: "))
    parameters['max_iterations'] = int(input("Enter the maximum number of iterations: "))
    parameters['ngh'] = int(input("Enter the neighborhood size (ngh): "))
    parameters['stlim'] = int(input("Enter the maximum number of iterations with no improvement (stlim): "))
    return parameters


# Function to perform local search to improve a given solution
def local_search(solution, n, ngh):
    """
    Perform local search to improve a given solution.

    Args:
    solution (list): List representing the positions of queens on the board.
    n (int): Size of the chessboard (number of queens).
    ngh (int): Neighborhood size.

    Returns:
    list: Improved solution after local search.
    """
    for col in range(n):
        min_conflicts = n
        best_row = solution[col]
        # Explore the neighborhood of the current position within the range defined by ngh
        for row in range(max(1, solution[col] - ngh), min(n + 1, solution[col] + ngh + 1)):
            if row != solution[col]:
                solution[col] = row
                conflicts = cost(solution)
                if conflicts < min_conflicts:
                    min_conflicts = conflicts
                    best_row = row
        solution[col] = best_row
        if min_conflicts == 0:
            break  # Stop if we find a position with no conflicts
    return solution


# Main function implementing the Bees Algorithm for solving the N-Queens problem
def bees_algorithm(n, parameters, initial_positions=None):
    """
    Main function implementing the Bees Algorithm for solving the N-Queens problem.

    Args:
    n (int): Size of the chessboard (number of queens).
    parameters (dict): Dictionary containing algorithm parameters.
    initial_positions (list or None): List representing initial positions of queens.
                                      If None, positions are generated randomly.

    Returns:
    list: Best solution found.
    """
    num_scouts = parameters['num_scouts']
    num_best_sites = parameters['num_best_sites']
    num_bees_best_sites = parameters['num_bees_best_sites']
    num_other_sites = parameters['num_other_sites']
    num_bees_other_sites = parameters['num_bees_other_sites']
    max_iterations = parameters['max_iterations']
    ngh = parameters['ngh']
    stlim = parameters['stlim']

    # Get initial positions of queens
    scout_solutions = [get_initial_positions(n, initial_positions) for _ in range(num_scouts)]
    best_solution = None
    best_cost = float('inf')
    some_interval = 10  # Interval for neighborhood shrinking
    some_threshold = 2  # Threshold for site abandonment
    no_improvement_runs = 0
    max_no_improvement_runs = stlim  # Maximum runs without improvement before stopping

    for iteration in range(max_iterations):
        # Evaluate all scout solutions
        costs = [cost(solution) for solution in scout_solutions]
        sorted_solutions = sorted(zip(scout_solutions, costs), key=lambda x: x[1])

        # Select best sites and perform local search
        best_sites = sorted_solutions[:num_best_sites]
        for i, (solution, solution_cost) in enumerate(best_sites):
            for _ in range(num_bees_best_sites):
                improved_solution = local_search(solution, n, ngh)
                improved_cost = cost(improved_solution)
                if improved_cost < solution_cost:
                    best_sites[i] = (improved_solution, improved_cost)
                    solution_cost = improved_cost

        # Select other sites and perform local search
        other_sites = sorted_solutions[num_best_sites:num_best_sites + num_other_sites]
        for i, (solution, solution_cost) in enumerate(other_sites):
            for _ in range(num_bees_other_sites):
                improved_solution = local_search(solution, n, ngh)
                improved_cost = cost(improved_solution)
                if improved_cost < solution_cost:
                    other_sites[i] = (improved_solution, improved_cost)
                    solution_cost = improved_cost

        # Combine best and other sites after local search
        combined_sites = best_sites + other_sites
        combined_solutions = [site for site, _ in combined_sites]
        combined_costs = [cost for _, cost in combined_sites]

        # Abandon sites that have not improved past the threshold
        combined_sites = [(solution, cost) for solution, cost in zip(combined_solutions, combined_costs) if
                          cost <= some_threshold]
        # Check for early termination if no improvement is observed
        if combined_sites:
            current_best_solution, current_best_cost = min(combined_sites, key=lambda x: x[1])
            if current_best_cost < best_cost:
                best_solution, best_cost = current_best_solution, current_best_cost
                no_improvement_runs = 0
                if best_cost == 0:  # Terminate if a valid solution is found
                    break
            else:
                no_improvement_runs += 1
                if no_improvement_runs >= max_no_improvement_runs:
                    break
        else:
            # If all sites are abandoned, regenerate the scout solutions
            scout_solutions = [get_initial_positions(n) for _ in range(num_scouts)]
            continue

        # Perform neighborhood shrinking at certain intervals
        if iteration % some_interval == 0 and iteration > 0:
            for i in range(len(scout_solutions)):
                scout_solutions[i] = local_search(scout_solutions[i], n, ngh)

        # Replace abandoned sites with new random scouts
        while len(combined_sites) < num_best_sites + num_other_sites:
            new_scout = heuristic_initial_positions(n)
            new_cost = cost(new_scout)
            combined_sites.append((new_scout, new_cost))

        # Update scout solutions with the combined sites
        scout_solutions = [solution for solution, cost in combined_sites]

    return best_solution


# Function to check if a solution is valid (no queens are attacking each other)
def is_solution_valid(position):
    """
    Check if a solution is valid (no queens are attacking each other).

    Args:
    position (list): List representing the positions of queens on the board.

    Returns:
    bool: True if the solution is valid, False otherwise.
    """
    n = len(position)
    if len(set(position)) != n:
        return False
    for i in range(n):
        for j in range(i + 1, n):
            if abs(position[i] - position[j]) == abs(i - j):
                return False
    return True


# Function to display the solution on a chessboard
def display_solution(solution):
    """Function to display the N-Queens solution on a chessboard.

    Arguments:
    - solution: A list representing the positions of queens on the board.

    Returns:
    - None
    """
    n = len(solution)
    print("Chessboard with Queens (Q) placed based on the solution:")
    top_border = "┌" + "───┬" * (n - 1) + "───┐"
    middle_border = "├" + "───┼" * (n - 1) + "───┤"
    bottom_border = "└" + "───┴" * (n - 1) + "───┘"

    print(top_border)
    for row in range(n):
        row_display = "│"
        for col in range(1, n + 1):
            if solution[col - 1] == row + 1:
                row_display += " Q │"
            else:
                row_display += "   │"
        print(row_display)
        if row < n - 1:
            print(middle_border)
    print(bottom_border)


# Example usage of the algorithm
if __name__ == "__main__":
    # Input board size with validation
    while True:
        try:
            n = int(input("Enter the board size (n): "))
            if n < 4:
                print("Board size must be 4 or greater.")
            else:
                break
        except ValueError:
            print("Invalid input. Please enter an integer.")

    # Ask if the user wants to input initial positions manually
    initial_positions_choice = input("Do you want to input the initial positions manually? (y/n): ").lower()
    if initial_positions_choice == 'y':
        initial_positions = get_initial_positions(n)
    else:
        initial_positions = None

    # Get algorithm parameters from the user
    parameters = get_algorithm_parameters()

    # Run the Bee Algorithm
    start_time = time.time()  # Start timing
    print("Running the Bee Algorithm...")
    best_solution = bees_algorithm(n, parameters, initial_positions)
    end_time = time.time()  # End timing
    time_spent = end_time - start_time  # Calculate time spent

    if best_solution and is_solution_valid(best_solution):
        print("\nBest Solution Found:")
        print(f"Time spent: {time_spent:.2f} seconds")
        print("Positions of Queens:", best_solution)  # Print the list of positions
        display_solution(best_solution)
        # print(f"Cost: {cost(best_solution)}")
    else:
        print("\nNo valid solution found.")
        print(f"Time spent: {time_spent:.2f} seconds")
