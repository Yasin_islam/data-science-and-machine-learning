import random
from itertools import accumulate
import time


def display_solution(solution):
    """
    Visual representation of the N-Queens solution with an enhanced chessboard.

    Arguments:
    solution (list): A list representing the positions of queens on the board.

    Returns:
    None
    """
    n = len(solution)
    print("Chessboard with Queens (Q) placed based on the solution:")
    top_border = "┌" + "───┬" * (n - 1) + "───┐"
    middle_border = "├" + "───┼" * (n - 1) + "───┤"
    bottom_border = "└" + "───┴" * (n - 1) + "───┘"

    print(top_border)
    for row in range(n):
        # Create each row based on the queen's position
        row_display = "│ " + " │ ".join('Q' if solution[col] == row else ' ' for col in range(n)) + " │"
        print(row_display)
        if row < n - 1:
            print(middle_border)
    print(bottom_border)


def is_valid_solution(solution):
    """Function to check if the given solution has any attacking queens.

    Arguments:
    - solution: A list representing the positions of queens on the board.

    Returns:
    - True if the solution is valid (no attacking queens), False otherwise.
    """
    n = len(solution)
    for i in range(n):
        for j in range(i + 1, n):
            if solution[i] == solution[j] or abs(solution[i] - solution[j]) == abs(i - j):
                return False
    return True


def get_user_input(n):
    """Function to prompt user for input regarding initial queen positions.

    Arguments:
    - n: An integer representing the size of the chessboard (number of queens).

    Returns:
    - A list of initial positions specified by the user, or None if not specified.
    """
    print("Welcome to the N-Queens Solver.")
    use_initial = input("Would you like to specify initial positions for queens? (yes/no): ").strip().lower()
    initial_positions = None
    if use_initial.startswith('y'):
        initial_positions_str = input(
            "Please enter the initial positions for the queens (0-indexed), separated by spaces: ").strip()
        initial_positions = [int(pos) for pos in initial_positions_str.split()]
        if len(initial_positions) != n:
            print(
                "Warning: The number of specified positions does not match the board size. Proceeding without initial positions.")
            initial_positions = None
    return initial_positions


class NQueensACO:
    def __init__(self, n, num_ants, evaporation_rate, pheromone_influence, heuristic_influence, iterations):
        """Constructor for the NQueensACO class.

        Arguments:
        - n: An integer representing the size of the chessboard (number of queens).
        - num_ants: An integer representing the number of ants in the colony.
        - evaporation_rate: A float representing the rate at which pheromone evaporates.
        - pheromone_influence: A float representing the influence of pheromones on ant movement.
        - heuristic_influence: A float representing the influence of heuristic information on ant movement.
        - iterations: An integer representing the number of iterations for the optimization algorithm.
        """
        self.n = n
        self.num_ants = num_ants
        self.evaporation_rate = evaporation_rate
        self.pheromone_influence = pheromone_influence
        self.heuristic_influence = heuristic_influence
        self.iterations = iterations
        self.pheromone = [[1 for _ in range(n)] for _ in range(n)]  # Initialize pheromone levels

    def solve(self, initial_positions=None):
        """Function to solve the N-Queens problem using Ant Colony Optimization.

        Arguments:
        - initial_positions: A list representing the initial positions of queens on the board (optional).

        Returns:
        - The best solution found by the algorithm (a list of queen positions).
        """
        best_solution = None
        best_fitness = float('-inf')

        for _ in range(self.iterations):
            solutions = self.construct_solutions(initial_positions)

            for solution in solutions:
                if is_valid_solution(solution):
                    fitness = self.calculate_fitness(solution)
                    if fitness > best_fitness:
                        best_fitness = fitness
                        best_solution = solution

            self.update_pheromone(solutions)

        return best_solution

    def construct_solutions(self, initial_positions=None):
        """Function to construct solutions using the Ant Colony Optimization algorithm.

        Arguments:
        - initial_positions: A list representing the initial positions of queens on the board (optional).

        Returns:
        - A list of solutions constructed by the ants.
        """
        solutions = []
        for _ in range(self.num_ants):
            solution = initial_positions.copy() if initial_positions else []
            while len(solution) < self.n:
                next_queen = self.select_next_queen(solution)
                if next_queen is None:
                    break
                solution.append(next_queen)
            if len(solution) == self.n:
                solutions.append(solution)
        return solutions

    def select_next_queen(self, partial_solution):
        """Function to select the next queen's position for the solution.

        Arguments:
        - partial_solution: A list representing a partial solution of queen positions.

        Returns:
        - The position for the next queen to be placed, or None if no valid position is found.
        """
        n = len(partial_solution)
        available_positions = list(range(self.n))
        random.shuffle(available_positions)
        for position in available_positions:
            if position not in partial_solution:
                # Check if the new position is not in the same column as any existing queen
                if all(row != position for row in partial_solution):
                    if self.is_non_attacking(partial_solution, position):
                        return position
        return None  # No valid position found for the next queen

    def is_non_attacking(self, solution, new_position):
        """Function to check if placing a queen in a new position causes any conflicts.

        Arguments:
        - solution: A list representing a solution of queen positions.
        - new_position: An integer representing the new position to be checked.

        Returns:
        - True if placing a queen in the new position does not cause any conflicts, False otherwise.
        """
        new_column = len(solution)
        for column, row in enumerate(solution):
            if row == new_position or \
                    abs(row - new_position) == abs(column - new_column):
                return False
        return True

    def update_pheromone(self, solutions):
        """Function to update pheromone trails based on the fitness of the solutions found.

        Arguments:
        - solutions: A list of solutions found by the ants.

        Returns:
        - None
        """
        for i in range(self.n):
            for j in range(self.n):
                self.pheromone[i][j] *= (1 - self.evaporation_rate)  # Evaporate pheromone
        for solution in solutions:
            if is_valid_solution(solution):
                for i, row in enumerate(solution):
                    self.pheromone[i][row] += 1  # Increase pheromone

    def calculate_fitness(self, solution):
        """Function to calculate fitness as the number of non-attacking pairs of queens.

        Arguments:
        - solution: A list representing a solution of queen positions.

        Returns:
        - The fitness value of the solution.
        """
        if not is_valid_solution(solution):
            return float('-inf')  # Invalid solutions have the worst possible fitness
        return 1


def main():
    n = int(input("Enter the board size (n): "))
    initial_positions = get_user_input(n)

    num_ants = int(input("Enter the number of ants: "))
    iterations = int(input("Enter the number of iterations: "))
    evaporation_rate = float(input("Enter the pheromone evaporation rate: "))
    pheromone_influence = float(input("Enter the pheromone influence (alpha): "))
    heuristic_influence = float(input("Enter the heuristic influence (beta): "))

    start_time = time.time()
    aco = NQueensACO(n, num_ants, evaporation_rate, pheromone_influence, heuristic_influence, iterations)
    solution = aco.solve(initial_positions)
    elapsed_time = time.time() - start_time

    if solution and is_valid_solution(solution):
        print(f"Solution found in {elapsed_time:.2f} seconds:")
        print("Positions of Queens:", solution)
        display_solution(solution)
    else:
        print("No valid solution found.")


if __name__ == "__main__":
    main()
