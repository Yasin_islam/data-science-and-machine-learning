import numpy as np
import random
import time

def objective_function(position):
    """
    Calculate the number of non-conflicting pairs of queens.

    Arguments:
    position (list): A list representing the positions of queens on the board.

    Returns:
    int: The negative count of conflicting queen pairs, maximizing this value minimizes conflicts.
    """
    n = len(position)
    conflicts = 0
    for i in range(n):
        for j in range(i + 1, n):
            if abs(i - j) == abs(position[i] - position[j]):
                conflicts += 1
    return -conflicts  # Maximizing the negative conflicts to minimize conflicts

def swap_positions(position):
    """
    Perform a swap to try and reduce conflicts.

    Arguments:
    position (list): A list representing the positions of queens on the board.

    Returns:
    list: A new position achieved by swapping two randomly selected queen positions.
    """
    new_position = position.copy()
    i, j = random.sample(range(len(position)), 2)  # Randomly select two positions for swapping
    new_position[i], new_position[j] = new_position[j], new_position[i]  # Swap positions
    return new_position

class Particle:
    def __init__(self, dimension, initial_position=None):
        """
        Initialize a particle with a position and its best position.

        Arguments:
        dimension (int): The number of queens on the board.
        initial_position (list or None): Optional. Initial position of the particle. If None, a random position is generated.

        Returns:
        None
        """
        if initial_position is not None:
            self.position = np.array(initial_position)
        else:
            self.position = np.random.permutation(dimension)  # Generate a random permutation as initial position
        self.best_position = np.copy(self.position)  # Initialize best position as current position
        self.best_score = objective_function(self.position)  # Evaluate the fitness of the initial position

def PSO(num_particles, dimension, num_iterations, w, c1, c2, initial_positions=None):
    """
    Perform Particle Swarm Optimization (PSO) to find a solution for the N-Queens problem.

    Arguments:
    num_particles (int): Number of particles (solutions) in the swarm.
    dimension (int): The number of queens on the board.
    num_iterations (int): Number of iterations (generations) for the optimization.
    w (float): Inertia weight parameter for PSO.
    c1 (float): Cognitive parameter for PSO.
    c2 (float): Social parameter for PSO.
    initial_positions (list or None): Optional. Initial positions of queens for each particle. If None, random positions are used.

    Returns:
    list or None: The final solution found by PSO (a list representing the positions of queens on the board), or None if no valid solution is found.
    """
    # Initialize particles with random positions or provided initial positions
    particles = [Particle(dimension, initial_positions) for _ in range(num_particles)]
    global_best = particles[0].position  # Initialize global best position
    global_best_score = objective_function(global_best)  # Evaluate the fitness of global best

    # Main PSO loop
    for _ in range(num_iterations):
        # Iterate over each particle
        for particle in particles:
            current_score = objective_function(particle.position)  # Evaluate the fitness of current position

            # Update the particle's best position if current position is better
            if current_score > particle.best_score:
                particle.best_position = np.copy(particle.position)
                particle.best_score = current_score

                # Update global best if current position is better than global best
                if current_score > objective_function(global_best):
                    global_best = np.copy(particle.position)

            # Simplified adaptation: Perform swaps that may reduce conflicts
            for _ in range(int(abs(w) + c1 + c2)):  # Adjust iteration based on PSO parameters
                candidate_position = swap_positions(particle.position)
                if objective_function(candidate_position) > current_score:
                    particle.position = candidate_position

    return global_best if is_valid_solution(global_best) else None

def is_valid_solution(position):
    """
    Check if the solution has no conflicts.

    Arguments:
    position (list): A list representing the positions of queens on the board.

    Returns:
    bool: True if the solution is valid (no conflicts), False otherwise.
    """
    return objective_function(position) == 0

def display_solution(solution):
    """
    Visual representation of the N-Queens solution with an enhanced chessboard.

    Arguments:
    solution (list): A list representing the positions of queens on the board.

    Returns:
    None
    """
    n = len(solution)
    print("Chessboard with Queens (Q) placed based on the solution:")
    top_border = "┌" + "───┬" * (n - 1) + "───┐"
    middle_border = "├" + "───┼" * (n - 1) + "───┤"
    bottom_border = "└" + "───┴" * (n - 1) + "───┘"

    print(top_border)
    for row in range(n):
        # Create each row based on the queen's position
        row_display = "│ " + " │ ".join('Q' if solution[col] == row else ' ' for col in range(n)) + " │"
        print(row_display)
        if row < n - 1:
            print(middle_border)
    print(bottom_border)

def get_user_input():
    """
    Prompt the user to input the board size and initial positions for queens (if desired).

    Returns:
    tuple: A tuple containing the board size and initial positions (if provided).
    """
    n = int(input("Enter the board size (n): "))
    choice = input("Do you want to specify initial positions for queens? [yes/no]: ").strip().lower()
    if choice == 'yes':
        initial_positions = list(map(int, input("Enter the initial positions for the queens, separated by spaces: ").split()))
    else:
        initial_positions = None
    return n, initial_positions

if __name__ == "__main__":
    n, initial_positions = get_user_input()
    num_particles = int(input("Enter the number of particles: "))
    num_iterations = int(input("Enter the number of iterations: "))
    w = float(input("Enter inertia weight (w): "))
    c1 = float(input("Enter Cognitive Parameter (c1): "))
    c2 = float(input("Enter social Parameter (c2): "))

    start_time = time.time()
    solution = PSO(num_particles, n, num_iterations, w, c1, c2, initial_positions)
    elapsed_time = time.time() - start_time
    print(f"Elapsed time: {elapsed_time:.2f} seconds.")

    if solution is not None and is_valid_solution(solution):
        print("A valid solution was found:")
        print("Positions of Queens:", [pos + 1 for pos in solution])  # Print the list of positions with 1 added to each position
        display_solution(solution)
    else:
        print("Failed to find a valid solution.")